# README #

Telit UE-866 G3 instructions

* author: Denis Shashunkin
* email:  d.shashunkin@yandex.ru


### Hardware installation instructions ###
1. connect the Telit UE-866 G3 board to PC with a usb cable
2. turn on the Telit UE-866 G3 board


### Drivers/Software installation instructions ###

1. need drivers and software are located in 3rd_party directory of this project
2. for Windows 8 and later install 3rd_praty/Telit_USB_Driver_Win_Desktop_UF.00.06_1.05.0003.zip drivers
   In other case or if there are any problem install 3rd_party/Telit_USB_Driver_Win_Desktop_UF.00.04.zip drivers
3. install Telit_XFP uility that is located in 3rd_party/Telit_XFP_Ver_4.0.5.zip
4. reboot PC


### Update firmware of the Telit UE-866 G3 module ###

1. check firmware version of the Telit UE-866 G3 module with help AT+CGMR command. If version is actual then do nothing.
2. turn off the Telit UE-866 G3 board
3. run Telit_XFP utility
4. In Telit_XFP utility select USB interface and firmware binary path ( located in 3rd_party )
5. Push Programm button
6. turn on the Telit UE-866 G3 board
7. wait until the programming is finished
8. if there are OK then Telit UE-866 G3 board updated succedd
9. check firmware version AT+CGMR


### Get custom software for the Telit UE-866 G3 module ###
1. git clone https://bitbucket.org/shashunkin/telit_ue866
2. cd telit_ue866
3. edit source code and save it
4. git commit
5. git push


### Upload custom software in the Telit UE-866 G3 module ###
1. copy source code in to 'fw' directory
2. copy configuration files in to 'cfg"
3. edit 'cfg/config' and 'cfg/phones'. Set correct values.
4. connect the Borad to a PC
5. edit 'Makefile.bat' for set correct ComPort for the Board
6. run 'Makefile.bat'
7. check correct loading script files by command at#lscript in a Terminal and check script files size


### Install AppZone IDE for developing custom software ###
1. download AppZone from https://www.telit.com/developer-zone/iot-app-zone/iot-app-zone-developer-resources/
2. install it
3. get the custom software project
4. open project int AppZone
5. add/remove functionality of the project
6. git commit
7. git push


### Description fields of the "config" configuration file ###
1. each line contains phone number in the template "+7xxxxxxxxxx"
2. max phone numbers is 3

### Description fields of the "phones" configuration file ###
1. version=ue806_r1_0.01 - version custom firmware
2. watchdog=500 - watchdog timer period, seconds
3. sync_time=21:00 - timestamp when is occurs sync time
4. sync_balance_time=07:00 - timestamp when is checking a balance
5. meter_interval=20 - sending metering data period
6. sim_pin= - sim pin
7. sim_ussd= - sim ussd
8. gprs_apn=internet.tele2.ru - gprs apn
9. gprs_login= - gprs login
10. gprs_pwd= - gprs password
11. time_zone=+03 - current timezone
12. min_balance=50 - minimal a balance when is sending to phones(prom 'phones') numbers
13. server_daytime=time.nist.gov - server datetime for sync time
14. server_daytime_port=13 - server datetime port
15. server_addr=narodmon.ru - server for sending metering data
16. server_port=8283 - port of metering server
17. timeout_on_start=50 - timeout custom firmware before start
18. timeout_before_reboot=5 - timeout before rebot
19. uart_speed=9600 - USIF1 speed
20. uart_dps=8N1 - USIF1 data/parity/stop bit
21. gpio_relay1=5 - gpio pin for relay 1
22. gpio_relay2=6 - gpio pin for relay 2
23. SCFG=1,1,1500,90,30,2 - config for gprs
24. SCFGEXT=1,1,0,0,0,0 - config for gprs
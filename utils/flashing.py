'''
Created on 15 Aptil 2015.

@author: Denis Shashunkin, d.shashunkin@yandex.ru

@target: send firmware to Telit ue-866 gprs module

'''

import sys
import serial
import time
import re
import os
import binascii
from multiprocessing.connection import answer_challenge
from Canvas import Line
import string

comPort = "COM19"

fw_dir_location = "fw"
cfg_dir_location = "cfg"
#list_fw_files = ["test"]
list_fw_files = [ "main", "at", "config", "date_time", "log", "relay", "sms", "tcp", "uart", "utils", "watchdog" ]
list_cfg_files = [ "config", "phones" ]
exp = ".py"



def GetLocalFileSize(filename):
    statinfo = os.stat(filename)
    return statinfo.st_size


def SendFile(filename):
    fd = open(filename, "rb")
    fileSize = GetLocalFileSize(filename)
    bytesRead = fd.read()
    if ( fileSize != len(bytesRead) ):
        print "filesize != bytesRead!!!"
        print fileSize
        print len(bytesRead)
        return None
    else:
        #print ' '.join(hex(ord(i)) for i in bytesRead)
        return bytesRead
    
    #for b in bytesRead:


def TryOpenComPort(port):
    try:
        return serial.Serial(
                port,
                baudrate=115200,
                timeout=1,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS
                )
    except:
        return None

def ReadComPortByTimeout(ser, timeout):
    spendTime = 0
    rxBuffer = []
    while (spendTime < timeout):
        rxString = ser.readline()
        #print "Length rxString = %u" % len(rxString)
        if ( len(rxString) > 0 ):
            if ( rxString == b"OK\r\n" or
                    rxString == b"ERROR\r\n" or
                        rxString == b"NO CARRIER\r\n" ):
#                    print "Got 'OK'"
                break
            
            elif ( rxString == b"\r\r\n" or 
                    rxString == b"\r\n" ):
                continue
            rxBuffer.append(rxString)
            #answerList += byte
            #print answerList
            
            #print rxBuffer
              
        time.sleep(0.1)
        spendTime += 0.1
        
    #print 'end ReadComPortByTimeout()'
    return rxBuffer

def SendDeleteFile( file, ser ):
    ser.write(b'AT#DSCRIPT=' + file + b"\r")
    print ReadComPortByTimeout(ser, 1)
    
    
def SendFileToModem( dir, fileName, ser ):
    ser.write(b'AT#WSCRIPT=' + '\"' + fileName + '\"' + ',' + str(GetLocalFileSize( dir + '/' + fileName)) + b"\r")
    answer = ReadComPortByTimeout(ser, 2)
    ser.write(SendFile(dir + '/' + fileName))
    ReadComPortByTimeout(ser, 4)

def main():
    print "Start writing firmware to Telit ue-866 module"
    
    #sys.stderr = sys.stdout
    
    ser = TryOpenComPort(comPort)
    if (ser == None):
        print "Error open %s" % comPort
        sys.exit(0)
    while( True ):
      ser.readall()
      time.sleep(0.5)
      ser.write(b"ATE1" + b"\r")
      time.sleep(0.5)
      rcv = ser.readall()
      if( 'OK'in rcv ):
        break;
      print "%s" % (ser.readall())
      time.sleep(0.5)

    ser.write(b"AT+CGMR" + b"\r")
    ver = ReadComPortByTimeout(ser, 2)
    print "Version module: {}".format(ver)

    for i in list_fw_files:
        print( "Writing fw : " + i )
        SendDeleteFile( i + ".py", ser )
        time.sleep( 1 )
        SendDeleteFile( i + ".pyo", ser )
        time.sleep( 1 )
        SendDeleteFile( i + ".pyc", ser )
        time.sleep( 1 )
        SendFileToModem( fw_dir_location, i + exp, ser )
        time.sleep( 1 )
    
    for i in list_cfg_files:
        print( "Writing cfg : " + i )
        SendDeleteFile( i, ser )
        time.sleep( 1 )
        SendFileToModem( cfg_dir_location, i, ser )
        time.sleep( 2 )

    ser.write(b'AT#STARTMODESCR=0' + b"\r")
    print ReadComPortByTimeout(ser, 1)
    
    ser.write(b'AT#ESCRIPT=' + list_fw_files[0] + exp + b"\r")
    print ReadComPortByTimeout(ser, 1)
    
    ser.write(b'AT#EXECSCR' + b"\r")
    print ReadComPortByTimeout(ser, 1)
    ser.close()
    sys.exit(0)


main()



